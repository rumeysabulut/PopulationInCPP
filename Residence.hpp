//
//  Residence.hpp
//  Project2
//
//  Created by Rumeysa Bulut on 2.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

#ifndef Residence_hpp
#define Residence_hpp

#include <string>
using namespace std;
class Residence{
private:
    int population;
    int minAge;
    int maxAge;
    string gender;
    string zipCode;
    string geo_id;
public:
    Residence();
    Residence(string);      //exceldeki bir sırayı alıp virgüle göre özellikleri ayıracak.
    int getPopulation();
    int getMinAge();
    int getMaxAge();
    string getGender();
    string getZipCode();
    string getGeo_id();
    void setPopulation(string);
    void setMinAge(string);
    void setMaxAge(string);
    void setGender(string);
    void setZipCode(string);
    void setGeo_id(string);
};

#endif /* Residence_hpp */
