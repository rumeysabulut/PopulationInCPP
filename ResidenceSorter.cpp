//
//  ResidenceSorter.cpp
//  Project2
//
//  Created by Rumeysa Bulut on 2.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

//IT'S DONE!!!
#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include "ResidenceSorter.hpp"
using namespace std;

ResidenceSorter::ResidenceSorter(const char *n){
    stringstream nValue;
    nValue << n;
    int recordNum;
    nValue >> recordNum;
    
    ifstream inputFile;
    inputFile.open("population_by_zip_2010.csv");
    string line;
    if(!inputFile)
        cout<< "Error opening input file" << endl;
    
    getline(inputFile,line);    //first line is the header so does nothing!
    int i=0;
    while(i < recordNum){       //reads first N records except header
        getline(inputFile, line);
        willBeSorted.push_back(Residence(line));
        i++;
    }
    /* for debugging
    for(int k = 0; k < willBeSorted.size(); k++){
        cout<<willBeSorted[k].getPopulation()<<","<<willBeSorted[k].getMinAge()<<","<<willBeSorted[k].getMaxAge()<<","<<willBeSorted[k].getGender()<<","<<willBeSorted[k].getZipCode()<<","<<willBeSorted[k].getGeo_id()<<endl;
    }   end debugging*/
    cout << "at" << endl;
    clock_t start = clock();
    quickSort(0, willBeSorted.size() - 1);
    clock_t end = clock();
    double elapsed_time = double (end - start) / CLOCKS_PER_SEC;
    cout << "Time elapsed for quick sort:" << elapsed_time << " seconds" << endl;

    writeToFile();
    /*cout<<"AFTER QUICK SORT"<<endl;
    for debugging
    for(int k=0; k< willBeSorted.size(); k++){
        cout<<willBeSorted[k].getPopulation()<<","<<willBeSorted[k].getMinAge()<<","<<willBeSorted[k].getMaxAge()<<","<<willBeSorted[k].getGender()<<","<<willBeSorted[k].getZipCode()<<","<<willBeSorted[k].getGeo_id()<<endl;
    }   end debugging*/
    
}


void ResidenceSorter::quickSort(long p, long r){
    long q;
    if( p < r ){
        q = partition(p, r);
        quickSort(p, q-1);
        quickSort(q+1, r);
    }
}

long ResidenceSorter::partition(long p, long r){
    Residence x = willBeSorted[r];
    Residence temp;
    
    long i = p-1;
    for(long j = p; j <= r-1; j++){
        if(willBeSorted[j].getPopulation() < x.getPopulation()){
            i = i+1;
            temp = willBeSorted[i];
            willBeSorted[i] = willBeSorted[j];
            willBeSorted[j] = temp;
        }
        else if(willBeSorted[j].getPopulation() == x.getPopulation()){
            if(willBeSorted[j].getGeo_id() < x.getGeo_id()){
                i = i+1;
                temp = willBeSorted[i];
                willBeSorted[i] = willBeSorted[j];
                willBeSorted[j] = temp;
            }
        }
        
    }
    temp = willBeSorted[i+1];
    willBeSorted[i+1] = willBeSorted[r];
    willBeSorted[r] = temp;
    
    return i+1;
}
void ResidenceSorter::writeToFile(){
    ofstream outputFile;
    outputFile.open("DataSorted.csv");
    if(!outputFile)
        cout<<"Error opening output file"<<endl;
    outputFile << "population,minimum_age,maximum_age,gender,zipcode,geo_id"<<endl;
    for(long i = 0; i < willBeSorted.size(); i++){
        outputFile << willBeSorted[i].getPopulation() << "," << willBeSorted[i].getMinAge() << "," << willBeSorted[i].getMaxAge() << "," << willBeSorted[i].getGender() << "," << willBeSorted[i].getZipCode() << "," << willBeSorted[i].getGeo_id() << endl;
        
    }
}
